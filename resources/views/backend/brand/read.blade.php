@extends('layouts.admin')

@section('content')
  
  <div class="col-12">
    <div class="card mb-3">
      <div class="card-header">
        <i class="fas fa-table"></i> Brand Table
        <span class="float-right"><a href="/createbrand" class="btn btn-outline-info"><i class="fas fa-plus"></i> Add New</a></span>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <th>No</th>
              <th>Brand Name</th>
              <th>Options</th>
            </thead>
            <tbody>
              <?php $i=1; ?>
              @foreach($brands as $brand)
                <tr>
                  <td><?php echo $i; ?></td>
                  <td>{{$brand->brand_name}}</td>
                  <td>
                    <a href="/brand/edit/{{$brand->id}}" class="btn btn-outline-warning">Edit</a>
                    <a href="/brand/delete/{{$brand->id}}" class="btn btn-outline-danger">Delete</a>
                  </td>
                </tr>
                <?php $i++; ?>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

@endsection


