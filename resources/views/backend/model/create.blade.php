@extends('layouts.admin')

@section('content')

<div class="col-sm-12">
	<form action="/createmodel" method="post" class="my-5">
		@csrf
		<h3 class="d-inline-block">Create New Model</h3>
		<a href="/brand" class="btn btn-outline-warning float-right"><i class="fas fa-backward"></i> Go Back</a>
		<div class="row my-5" >
			<div class="col-sm-3 form-group">
				<label for="name">Model Name:</label>
			</div>
			<div class="form-group col-sm-9">
				<input type="text" name="brand_name" class="form-control" id="name" autofocus="autofocus">
			</div>
		</div>
		<div class="row my-5" >
			<div class="col-sm-3 form-group">
				<label for="brandname">Model Name:</label>
			</div>
			<div class="form-group col-sm-9">
				<select name="brandid" id="brandname" class="form-control">
					@foreach($brands as $brand)
						<option value="{{$brand->id}}">{{$brand->brand_name}}</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="form-group my-5">
			<input type="submit" name="Create" class="btn btn-outline-success" style="width: 100px; height: 40px;" >
		</div>
		
	</form>
</div>

@endsection