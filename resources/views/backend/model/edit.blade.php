@extends('layouts.admin')

@section('content')

<div class="col-sm-12">
	<form action="/model/edit" method="post" class="my-5">
		@csrf
		<input type="hidden" name="editid" value="{{$model->id}}">
		<h3 class="d-inline-block">Create New Model</h3>
		<a href="/model" class="btn btn-outline-warning float-right"><i class="fas fa-backward"></i> Go Back</a>
		<div class="row my-5" >
			<div class="col-sm-3 form-group">
				<label for="name">Model No:</label>
			</div>
			<div class="form-group col-sm-9">
				<input type="text" name="brand_name" class="form-control" id="name" autofocus="autofocus" value="{{$brand->brand_name}}">
			</div>
		</div>
		<div class="form-group my-5">
			<input type="submit" name="Create" class="btn btn-outline-success" style="width: 100px; height: 40px;" >
		</div>
		
	</form>
</div>

@endsection