<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('layouts.admin');
});
*/
//admin
Route::get('/admin', 'Backend\AdminController@index');

//brands
Route::get('brand','Backend\BrandController@index');
Route::get('/createbrand','Backend\BrandController@create');
Route::post('/createbrand','Backend\BrandController@store');
Route::get('brand/delete/{brand}','Backend\BrandController@destroy');
Route::get('brand/edit/{brand}', 'Backend\BrandController@edit');
Route::post('/brand/edit', 'Backend\BrandController@update');

//models
Route::get('/createmodel', 'Backend\ModelController@create');
Route::post('/createmodel', 'Backend\ModelController@store');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
