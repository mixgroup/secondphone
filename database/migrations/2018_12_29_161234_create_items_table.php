<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item_name');
            $table->string('photo');
            $table->string('adapter');
            $table->string('earphone');
            $table->integer('seller_id')->unsigned();
            $table->integer('model_id')->unsigned();
            $table->string('price');
            $table->foreign('seller_id')->references('id')->on('sellers');
            $table->foreign('model_id')->references('id')->on('phone_models');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
